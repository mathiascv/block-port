Instrucciones de instalación:
	
	Windows:
		1) Instalar python 3.7.6 (marcar opción de agregarlo al path)
					https://www.python.org/ftp/python/3.7.6/python-3.7.6-amd64.exe
		2) Instalar Npcap 
					https://nmap.org/npcap/windows-10.html
		3) Instalar la terminal de linux:
			3.1) Activar el modo desarrollador: Settings> Update & Security > For Developers.
			3.2) Habilitar Windows Subsystem for Linux: Control Panel > Programs > Activar o desactivar las caracteristicas de windows. Marcar opción "Subsistema de Windows para Linux"
			3.3) Descargar terminal de Ubuntu
					https://www.microsoft.com/es-cl/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab
        4) Instalar Argus.
        	4.1) Abrir terminal Ubuntu instalada en el paso 3
        	4.2) Ejecutar los siguientes comandos en la terminal de Ubuntu:
        		sudo apt-get install build-essential
		        sudo apt install flex
		        sudo apt install bison
		        sudo apt-get update -y
		        sudo apt-get install -y libpcap-dev
		    4.3) Instalar argus server con los siguientes comandos:
		    	wget http://qosient.com/argus/dev/argus-3.0.8.2.tar.gz
		        tar zxf argus-3.0.8.2.tar.gz
		        cd argus-3.0.8.2
		        sudo ./configure
		        sudo make
		        sudo make install
		    4.4) Instalar argus client con los siguientes comandos:
		    	wget http://qosient.com/argus/dev/argus-clients-3.0.8.2.tar.gz
		        tar zxf argus-clients-3.0.8.2.tar.gz
		        cd argus-clients-3.0.8.2
		        sudo ./configure
		        sudo make
		        sudo make install
		    4.5) Cerrar terminal Ubuntu
		5) Instalar bibliotecas de python con pip
			0) En caso de no estar instalado pip 
						https://pypi.org/project/pip/
			1) pip install pandas
			2) pip install scapy
			3) pip install graypy
			4) pip install joblib
			5) pip install sklearn
			6) pip install xgboost
			7) pip install numpy==1.19.3

Instrucciones de uso del módulo AIDS:

	1) Abrir una terminal con permisos de administrador.
	2) Cambiar al directorio Sensor
	3) Ejecutar Sensor.py
		3.1) python sensor.py

Funcionalidades módulo AIDS:

	1) Sensor.py: Analiza la red según un tiempo determinado y genera archivos pcap
	2) Preprocessing.py: Ejecuta argus y genera archivos .csv y .argus con información de la red
	3) Monitoring.py: Accede al archivo .csv con data de ataques recientes y los clasifica según su tipo
	4) Analyzer.py: Analiza los datos generado por Monitoring y los compara según una ventana de tiempo y cantidad de veces presente.
		4.1) Ataque.py: Clase con atributos correspondiente a los ataques generados.
	5) Planner.py: En base a la información generada por Analyzer genera un plan de mitigación a los ataques detectados.
	6) Executer.py: Recibe un plan de mitigación desde Planner y ejecuta las acciones solicitadas
			6.1) Actuator.py: Decodifica el plan enviado por el Planer y ejecuta las funciones
			6.2) PortManagement.py: Contiene las funciones ejecutadas por Actuator.
			6.3) Log.py: Modulo para registro de acciones ejecutadas.


Estas funciones se encuentran a el directorio Obsoleto.
Se muestran las instrucciones en caso de que se quieran ejecutar, pero no es necesario para el funcionamiento del módulo AIDS	

Instrucciones de uso de los scripts:

	1) closePort.py:
		Windows:
			a) Abrir terminal con permisos de administrador
			b) Para ejecutar escribir en la terminal: python closePort.py [puerto]


	2) openPort.py
		Windows:
			a) Abrir terminal con permisos de administrador
			b) Para ejecutar escribir en la terminal: python openPort.py [puerto]


	3) scannerPort.py
		Windows:
			a) Abrir terminal con permisos de administrador
			b) Para ejecutar escribir en la terminal: python scannerPort.py [puerto] [IP]


	4) blockIP.py
		Windows:
			a) Abrir terminal con permisos de administrador
			b) Para ejecutar escribir en la terminal: python blockIP.py [IP]

	5) responseScript.py
		Windows:
			a) Abrir terminal con permisos de administrador
			b) Para ejecutar los scripts se debe seguir el siguiente formato:
				Número [flags separadas por espacio]
				Número [flags separadas por espacio]; Número2 [flags separadas por espacio]
				b.1) número: Número de 1 a 4; Cada script está asociado a un número.
					1) blockIP.py
					2) closePort.py 
					3) openPort.py
					4) scannerPort.py
				b.2) Flags: Seguido del número de script y separadas por un espacio, van las flags de cada archivo.
				b.3) Para ejecutar varios scripts debe separarse por un ; cada linea.
				Ejemplo:
					a) 1 180.160.0.1
					b) 2 443; 4 443 www.google.com; 3 443
			c) Threading: para habilitar la ejecución en paralelo se utiliza la flag -m.
				Número [flags separadas por espacio]; -m
				Número [flags separadas por espacio]; -m; Número2 [flags separadas por espacio]
				Ejemplo:
					a) 1 180.160.0.1; 2 443; -m
					b) -m; 2 443; 4 443 www.google.com; 3 443
				Nota: No importa la posición de -m.


Instrucciones de uso de código orientado a objetos:
	
	1) main.py:
		Windows:
			a) Abrir terminal con permiso de administrador
			b) Para ejecutar en la terminar: python main.py [puerto/IP] -[flag]
			c) Hay 5 flags: -h, -o, -c, -s, -B
				c.1) -h: Muestra ayuda sobre las otras flags. Ej: python main.py -h
				c.2) -o: Se utiliza para abir un puerto	Ej: python main.py [puerto] -o
				c.3) -c: Se utiliza para cerrar un puerto. Ej: python main.py [puerto] -c
				c.4) -s: Se utiliza para scanear un puerto Ej: python main.py [puerto] -s [IP]
				c.5) -B: Se utiliza para bloquear una IP. Ej: python main.py [IP] -B

Instrucciones de uso Response.py:

	Windows:
		a) Abrir terminal con permisos de administrador
		b) Para ejecutar los scripts se debe seguir el siguiente formato:
			NombreFunción Puerto/IP [flags separadas por espacio]
			NombreFunción Puerto/IP [flags separadas por espacio]; NombreFunción PUERTO2/IP2 [flags separadas por espacio]
			b.1) Puerto/IP: Puerto objetivo o IP objetivo
			b.2) NombreFunción: Seguido del puerto o IP y separadas por un espacio, van las flags de cada funcionalidad.
				Hay 4 funciones
				b.1) OpenPort: Se utiliza para abir un puerto.
				b.2) ClosePort: Se utiliza para cerrar un puerto.
				b.3) ScanPort: Se utiliza para scanear un puerto. Ej: Puerto -s IP
				b.4) BlockIp: Se ultiliza para bloquear una IP. 
			b.3) Para ejecutar varias funciones debe separarse por un ; cada linea.
			Ejemplo:
				a) ClosePort 443
				b) ScanPort 443 www.google.com; ClosePort 443; OpenPort 500
		c) Threading: para habilitar la ejecución en paralelo se utiliza la flag -m seguido de las funciones y sus argumentos entre parentesis () y separados con una ,.
			-m (NombreFunción Puerto/IP [flags separadas por espacio], NombreFunción2 PUERTO2/IP2 [flags separadas por espacio]); NombreFunción3 PUERTO3/IP3 [flags separadas por espacio]
			Nota: NombreFunción y NombreFunción2 serán ejecutadas en paralelo mientras que NombreFunción3 será ejecutada de manera secuencial.
			Ejemplo:
				a) ClosePort 443; OpenPort 443
				b) -m (ClosePort 443, BlockIP 1.1.1.1); OpenPort 400
				c) -m (ScanPort 443 www.google.com, ClosePort 443, OpenPort 500)
	
Funcionalidades:

	1) closePort.py:
		Crea una regla en el Firewall que bloquea las comunicaciones salientes con el puerto dado
	2) openPort.py
		Elimina una regla previamente creada.
	3) scannerPort.py
		Escanea un puerto específico para saber si está abierto o cerrado para cierta dirección IP
	4) blockIP.py
		Bloquea un ip para que no pueda realizar conexiones con el servidor.
	5) responseScript.py
		Ejecuta por comando los scripts señalados en paralelo o secuencialmente.		
	6) main.py 
		Ejecuta las mismas funciones diferenciando por las flags.
	7) Response.py
		Recibe instrucciones para la ejecución de de las fuciones de main.py. Soporta ejecución secuencial y en paralelo.
	8) LogModule.py:
		Genera un archivo log con las funciones ejecutadas.

