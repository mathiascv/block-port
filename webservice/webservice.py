from flask import Flask
from flask import jsonify

FILE = "C:\\Users\\mcabr\\Desktop\\PT\\AIDS\\AIDS\\Monitoring\\log.txt"

app = Flask(__name__)

@app.route("/getAtaques", methods=['GET'])
def getAtaques():
    return jsonify(getData(FILE))

def getData(ruta):
    data = []
    file = open(ruta, "r")
    for linea in file:
        info = linea.split()
        t = (info[1], info[3], info[5], info[7], info[9]) #[0] puerto, [1] tipo, [2] fecha, [3] hora, [4] ip
        if t[1] == "DoS" or t[1] == "Fuzzers":
           puerto = t[4]
        else: puerto = t[0]

        dic = {
            'port': puerto,
            'tipo': t[1],
            'fecha': t[2],
            'hora': t[3],
            'ip': t[4]
        }
        data.append(dic)
    file.close()
    return data

if __name__=='__main__':
    app.run()
